﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab
{
    class Programs
    {
        public class DelList
        {
            public string ID { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string Code { get; set; }
            public string Budget { get; set; }
            public string Used { get; set; }

        }
        static void Main(string[] args)
        {

            QryData();
            InsertData();
            Update();
            Delete();
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }

        static public void QryData()
        {
            using (var db = new workshopLinq())
            {
                string CountryCode = "US";
                var ds = (from a in db.CUSTOMER
                          join b in db.COUNTRY on new { a.COUNTRY_CODE }
                          equals new { b.COUNTRY_CODE }
                          where a.COUNTRY_CODE == CountryCode
                          select new
                          {
                              CusID = "ID: " + a.CUSTOMER_ID,
                              CusName = "Name: " + a.NAME,
                              CusEmail = "Email: " + a.EMAIL,
                              CountryName = "Country: " + b.COUNTRY_NAME,
                              Budget = "Budget: " + a.BUDGET,
                              Used = "Used: " + a.USED
                          }).ToList();

                if (ds.Count() > 0)
                {
                    foreach (var item in ds)
                    {
                        Console.WriteLine(String.Format("{0} {1} {2} {3} {4} {5}"
                            , item.CusID
                            , item.CusName
                            , item.CusEmail
                            , item.CountryName
                            , item.Budget
                            , item.Used));
                    }
                }
            }
            
            

        }

        static public void InsertData()
        {
            using (var db = new workshopLinq())
            {
                db.CUSTOMER.Add(new CUSTOMER()
                {
                    CUSTOMER_ID = "C005",
                    NAME = "Rut Wisarut",
                    EMAIL = "rut.wisarut@thaicreate.com",
                    COUNTRY_CODE = "TH",
                    BUDGET = 5000000,
                    USED = 0,
                });

                db.SaveChanges();

            }
        }

        static public void Update()
        {
            using (var db = new workshopLinq())
            {
                string nameUpdate = "j";
                var update = db.CUSTOMER.Where(o => o.NAME.Contains(nameUpdate)).ToList();
                    
                if(update != null)
                {
                   foreach(var item in update)
                    {
                        item.BUDGET = 0;
                        item.USED = 1;
                    }
                }

                db.SaveChanges();

            }
        }

        static public void Delete()
        {
            using (var db = new workshopLinq())
            {
                string compareString = "";
                var ds = (from a in db.CUSTOMER
                          select a).ToList();
                if (ds.Count > 0)
                {
                    foreach(var item in ds)
                    {
                        compareString = item.NAME;
                    }
                    Console.WriteLine(compareString);
                    db.CUSTOMER.Remove(db.CUSTOMER.Where(o => o.NAME == compareString).FirstOrDefault());

                    db.SaveChanges();
                }
            }
        }
  
    }
}
